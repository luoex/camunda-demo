package com.luo.camunda.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.luo.camunda.app.constants.PaymentProcessConstants;
import com.luo.camunda.app.enums.ConfirmResultEnum;
import com.luo.camunda.app.enums.PaymentResultEnum;
import com.luo.camunda.app.model.entity.BizPaymentProcessInfo;
import com.luo.camunda.common.constants.ProcessConstants;
import com.luo.camunda.common.model.vo.TaskVo;
import com.luo.demo.sc.base.enums.RespCodeEnum;
import com.luo.demo.sc.base.model.result.RespResult;
import com.luo.demo.sc.base.test.BaseTest;
import com.luo.demo.sc.base.test.JsonFileSource;
import com.luo.demo.sc.base.utils.JsonUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * camunda - 支付流程 - 测试
 *
 * @author luohq
 * @date 2022-01-30 14:09
 */
@TestMethodOrder(MethodOrderer.MethodName.class)
public class CamundaPaymentApplicationTest extends BaseTest {


    private final String BASE_PATH = "/payment";

    @ParameterizedTest
    @JsonFileSource(jsonKey = "$.startPayment")
    void test01_startPayment(String paymentRequestJson) throws Exception {
        this.mockMvc.perform(post(BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(paymentRequestJson))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test02_getTasks() throws Exception {
        this.mockMvc.perform(get(BASE_PATH + "/tasks")
                .queryParam(PARAM_ORDER_BY, ProcessConstants.COLUMN_CREATE_TIME)
                .queryParam(PARAM_ORDER_SORT, ProcessConstants.SORT_DESC)
                .queryParam("assignee", "luo")
                .queryParam("processDefinitionKey", PaymentProcessConstants.PAYMENT_PROCESS_ID)
                .queryParam("taskDefinitionKey", PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK_ID)
                .queryParam("withProcessVariables", "true"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test03_confirmPayment() throws Exception {
        //查询用户的待处理任务列表
        String taskListResultJson = mockMvc.perform(get(BASE_PATH + "/tasks")
                .queryParam(PARAM_ORDER_BY, ProcessConstants.COLUMN_CREATE_TIME)
                .queryParam(PARAM_ORDER_SORT, ProcessConstants.SORT_DESC)
                .queryParam("assignee", "luo")
                .queryParam("processDefinitionKey", PaymentProcessConstants.PAYMENT_PROCESS_ID)
                .queryParam("taskDefinitionKey", PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK_ID)
                .queryParam("withProcessVariables", "true"))
                .andDo(print())
                .andReturn()
                .getResponse().getContentAsString();
        //获取最近创建的待处理任务
        RespResult<TaskVo<BizPaymentProcessInfo>> respResult = JsonUtils.fromJson(taskListResultJson, new TypeReference<RespResult<TaskVo<BizPaymentProcessInfo>>>() {});
        TaskVo<BizPaymentProcessInfo> curTaskVo = respResult.getRows().get(0);
        Assertions.assertNotNull(curTaskVo);

        //拼接用户确认参数
        String confirmJsonParamFormat = "{\"taskId\":\"%s\", \"processInstanceId\":\"%s\", \"bizKey\": \"%d\", \"approvalResult\": %d}";
        String confirmJsonParam = String.format(confirmJsonParamFormat,
                curTaskVo.getId(),
                curTaskVo.getProcessInstanceId(),
                curTaskVo.getBizProcessData().getId(),
                1);
        //调用确认接口
        this.mockMvc.perform(put(BASE_PATH + "/confirm")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(UTF_8)
                .content(confirmJsonParam))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test04_getHistoryTasks() throws Exception {
        this.mockMvc.perform(get(BASE_PATH + "/tasks/history")
                .queryParam(PARAM_ORDER_BY, ProcessConstants.COLUMN_START_TIME)
                .queryParam(PARAM_ORDER_SORT, ProcessConstants.SORT_DESC)
                .queryParam("assignee", "luo")
                .queryParam("processDefinitionKey", PaymentProcessConstants.PAYMENT_PROCESS_ID)
                //.queryParam("taskDefinitionKey", PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK_ID)
                .queryParam("withProcessVariables", "true"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test05_getPayments_all() throws Exception {
        this.mockMvc.perform(get(BASE_PATH)
                .queryParam(PARAM_PAGE_NO, "1")
                .queryParam(PARAM_PAGE_SIZE, "10")
                .queryParam(PARAM_ORDER_BY, "createTime")
                .queryParam(PARAM_ORDER_SORT, "desc"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test06_getPayments_not_confirm() throws Exception {
        this.mockMvc.perform(get(BASE_PATH)
                .queryParam(PARAM_PAGE_NO, "1")
                .queryParam(PARAM_PAGE_SIZE, "10")
                .queryParam(PARAM_ORDER_BY, "createTime")
                .queryParam(PARAM_ORDER_SORT, "desc")
                .queryParam("approvalResult", ConfirmResultEnum.NOT_CONFIRM.getCode().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }

    @Test
    void test07_getPayments_with_condition() throws Exception {
        this.mockMvc.perform(get(BASE_PATH)
                .queryParam(PARAM_PAGE_NO, "1")
                .queryParam(PARAM_PAGE_SIZE, "10")
                .queryParam(PARAM_ORDER_BY, "createTime")
                .queryParam(PARAM_ORDER_SORT, "desc")
                .queryParam("approvalResult", ConfirmResultEnum.CONFIRM_PAY.getCode().toString())
                .queryParam("paymentResult", PaymentResultEnum.PAY_SUCCESS.getCode().toString())
                .queryParam("productName", "吉他")
                .queryParam("productDiscountPriceStart", "1000")
                .queryParam("productDiscountPriceEnd", "5000")
                .queryParam("paymentTimeStart", "2022-01-01 00:00:00")
                .queryParam("paymentTimeEnd", "2022-12-31 12:03:32")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(JSON_PATH_RESP_CODE).value(RespCodeEnum.SUCCESS.getCode()));
    }
}
