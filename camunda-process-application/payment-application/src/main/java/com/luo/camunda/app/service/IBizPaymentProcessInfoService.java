package com.luo.camunda.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.luo.camunda.app.model.entity.BizPaymentProcessInfo;
import com.luo.camunda.app.model.param.PaymentConfirmParam;
import com.luo.camunda.app.model.param.PaymentQueryParam;
import com.luo.camunda.app.model.param.PaymentRequestParam;
import com.luo.camunda.common.model.param.TaskQueryParam;
import com.luo.camunda.common.model.vo.TaskVo;
import com.luo.demo.sc.base.model.result.RespResult;
import org.camunda.bpm.engine.history.HistoricTaskInstance;

/**
 * 支付流程 - 业务数据 服务类
 *
 * @author luohq
 * @since 2022-01-30
 */
public interface IBizPaymentProcessInfoService extends IService<BizPaymentProcessInfo> {

    /**
     * 开启支付流程
     *
     * @param paymentRequestParam 支付请求参数
     * @return 响应结果
     */
    RespResult<String> startPaymentProcess(PaymentRequestParam paymentRequestParam);

    /**
     * 查询待处理任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    RespResult<TaskVo<BizPaymentProcessInfo>> queryTasks(TaskQueryParam taskQueryParam);

    /**
     * 查询历史任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    RespResult<HistoricTaskInstance> queryHistoryTasks(TaskQueryParam taskQueryParam);

    /**
     * 确认支付
     *
     * @param paymentConfirmParam 确认参数
     * @return 响应结果
     */
    RespResult confirmPayment(PaymentConfirmParam paymentConfirmParam);

    /**
     * 查询支付业务数据列表
     *
     * @param paymentQueryParam 查询参数
     * @return 响应结果
     */
    RespResult<BizPaymentProcessInfo> queryPayments(PaymentQueryParam paymentQueryParam);
}
