package com.luo.camunda.app.model.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 支付确认参数
 *
 * @author luohq
 * @date 2022-01-22 18:05
 */
@Data
public class PaymentConfirmParam {
    /**
     * 任务ID
     */
    @NotBlank
    private String taskId;
    /**
     * 流程实例ID
     */
    @NotBlank
    private String processInstanceId;
    /**
     * 业务KEY
     */
    @NotBlank
    private String bizKey;
    /**
     * 用户是否同意付款(0:未确认,1:同意, 2:不同意)
     */
    @NotNull
    private Integer approvalResult;
}
