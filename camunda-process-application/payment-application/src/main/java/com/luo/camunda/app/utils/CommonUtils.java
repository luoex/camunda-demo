package com.luo.camunda.app.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.luo.demo.sc.base.model.param.BasePageParam;
import com.luo.demo.sc.base.model.result.RespResult;

import java.util.Arrays;
import java.util.List;

/**
 * 通用工具类
 *
 * @author luohq
 * @date 2022-01-31 12:07
 */
public class CommonUtils {

    private static List<String> DESC_SORT = Arrays.asList("desc", "DESC");

    public static Page convertPage(BasePageParam basePageParam) {
        Page page = new Page();
        page.setCurrent(basePageParam.getPageNo());
        page.setSize(basePageParam.getPageSize());
        if (null != basePageParam.getOrderBy()) {
            String underlineOrderBy = StringUtils.camelToUnderline(basePageParam.getOrderBy());
            page = DESC_SORT.contains(basePageParam.getOrderSort())
                    ? page.addOrder(OrderItem.desc(underlineOrderBy))
                    : page.addOrder(OrderItem.asc(underlineOrderBy));
        }
        return page;
    }

    public static <T> RespResult<T> convertPageResult(IPage<T> pageResult) {
        return RespResult.successRows(pageResult.getTotal(), pageResult.getRecords());
    }
}
