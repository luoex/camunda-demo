package com.luo.camunda.app.model.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 支付请求参数
 *
 * @author luohq
 * @date 2022-01-22 18:05
 */
@Data
public class PaymentRequestParam {
    /**
     * 商品名称
     */
    @NotBlank
    private String productName;
    /**
     * 商品价格
     */
    @NotNull
    private BigDecimal productPrice;
    /**
     * 支付用户ID
     */
    @NotBlank
    private String paymentAssignee;
}
