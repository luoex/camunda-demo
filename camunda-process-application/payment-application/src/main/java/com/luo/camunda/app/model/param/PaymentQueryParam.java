package com.luo.camunda.app.model.param;

import com.luo.demo.sc.base.model.param.BasePageParam;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 支付确认参数
 *
 * @author luohq
 * @date 2022-01-22 18:05
 */
@Data
public class PaymentQueryParam extends BasePageParam {
    /**
     * 业务主键
     */
    private Long id;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品金额
     */
    private BigDecimal productPriceStart;
    private BigDecimal productPriceEnd;

    /**
     * 商品折扣
     */
    private BigDecimal productDiscountStart;
    private BigDecimal productDiscountEnd;

    /**
     * 商品折扣后金额
     */
    private BigDecimal productDiscountPriceStart;
    private BigDecimal productDiscountPriceEnd;

    /**
     * 用户是否同意付款(0:未确认,1:同意, 2:不同意)
     */
    private Integer approvalResult;

    /**
     * 支付用户ID
     */
    private String paymentAssignee;
    /**
     * 支付结果（0:未支付,1:成功, 2:失败）
     */
    private Integer paymentResult;

    /**
     * 创建时间
     */
    private LocalDateTime createTimeStart;
    private LocalDateTime createTimeEnd;

    /**
     * 用户确认时间
     */
    private LocalDateTime approvalTimeStart;
    private LocalDateTime approvalTimeEnd;

    /**
     * 用户支付时间
     */
    private LocalDateTime paymentTimeStart;
    private LocalDateTime paymentTimeEnd;
}
