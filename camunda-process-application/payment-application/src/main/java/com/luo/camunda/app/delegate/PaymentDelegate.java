package com.luo.camunda.app.delegate;

import com.luo.camunda.app.enums.PaymentResultEnum;
import com.luo.camunda.app.model.wrapper.PaymentProcessVariablesWrapper;
import com.luo.camunda.app.model.entity.BizPaymentProcessInfo;
import com.luo.camunda.app.service.IBizPaymentProcessInfoService;
import com.luo.demo.sc.base.execption.MsgRuntimeException;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * 支付活动 - Java代理
 *
 * @author luohq
 * @date 2022-01-31
 */
@Component
@Slf4j
public class PaymentDelegate implements JavaDelegate {

    @Resource
    private IBizPaymentProcessInfoService bizPaymentProcessInfoService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void execute(DelegateExecution execution) {
        /** 获取流程相关信息 */
        String bizKey = execution.getProcessBusinessKey();
        String processInstanceId = execution.getProcessInstanceId();
        log.info("调用支付服务，processInstanceId: {}, bizKey: {}", processInstanceId, bizKey);

        PaymentProcessVariablesWrapper paymentProcessVariablesWrapper = new PaymentProcessVariablesWrapper(execution.getVariables());
        log.info("RPC调用支付服务, 流程变量：{}", paymentProcessVariablesWrapper);

        /** 更新业务DB */
        BizPaymentProcessInfo bizPaymentProcessInfo = BizPaymentProcessInfo.builder()
                .id(Long.valueOf(bizKey))
                .paymentResult(PaymentResultEnum.PAY_SUCCESS.getCode())
                .paymentTime(LocalDateTime.now())
                .build();
        log.info("更新支付结果, 参数：{}", bizPaymentProcessInfo);
        Boolean result = this.bizPaymentProcessInfoService.updateById(bizPaymentProcessInfo);
        log.info("更新支付结果, 结果：{}", result);
        //抛出异常，则此task执行失败，回退到上一步
        //throw new MsgRuntimeException("支付异常");
    }

}