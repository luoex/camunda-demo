package com.luo.camunda.app.enums;

/**
 * 支付结果枚举
 *
 * @author luohq
 * @date 2022/01/30
 */
public enum PaymentResultEnum {
    NOT_PAY(0, "未支付"),
    PAY_SUCCESS(1, "支付成功"),
    PAY_FAIL(2, "支付失败");

    private Integer code;
    private String message;

    PaymentResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * code是否相等
     *
     * @param code
     * @return
     */
    public Boolean equalCode(Integer code) {
        for (PaymentResultEnum curEnum : values()) {
            if (curEnum.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据code转换为枚举类
     *
     * @param code
     * @return
     */
    public static PaymentResultEnum ofCode(String code) {
        for (PaymentResultEnum curEnum : values()) {
            if (curEnum.getCode().equals(code)) {
                return curEnum;
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
