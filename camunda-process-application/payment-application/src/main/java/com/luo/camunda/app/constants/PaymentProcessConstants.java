package com.luo.camunda.app.constants;

/**
 * 流程常量定义
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-01-25 10:47
 */
public class PaymentProcessConstants {
    /**
     * 流程定义ID
     */
    public static final String PAYMENT_PROCESS_ID = "PaymentProcess";
    /**
     * 用户确认任务ID
     */
    public static final String PAYMENT_USER_CONFIRM_TASK_ID = "Task_UserConfirmPayment";
    /**
     * 流程变量名
     */
    public static final String PAYMENT_APPROVAL_RESULT_VAR_NAME = "approvalResult";
    public static final String PAYMENT_PRODUCT_NAME_VAR_NAME = "productName";
    public static final String PAYMENT_PRODUCT_PRICE_VAR_NAME = "productPrice";
    public static final String PAYMENT_PRODUCT_DISCOUNT_RESULT_VAR_NAME = "productDiscountResult";
    public static final String PAYMENT_PRODUCT_DISCOUNT_VAR_NAME = "productDiscount";
    public static final String PAYMENT_PRODUCT_DISCOUNT_PRICE_VAR_NAME = "productDiscountPrice";



}
