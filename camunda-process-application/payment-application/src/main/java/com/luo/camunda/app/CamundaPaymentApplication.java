package com.luo.camunda.app;

import com.luo.camunda.common.anno.EnableCamundaCommon;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.camunda.bpm.spring.boot.starter.event.PreUndeployEvent;
import org.camunda.bpm.spring.boot.starter.event.TaskEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.EventListener;

@Slf4j
@SpringBootApplication
@EnableProcessApplication
@EnableCamundaCommon
public class CamundaPaymentApplication {

    boolean processApplicationStopped;

    public static void main(final String... args) throws Exception {
        SpringApplication.run(CamundaPaymentApplication.class, args);
    }

    @EventListener
    public void onPostDeploy(PostDeployEvent event) {
        log.info("postDeploy: {}", event);
    }

    @EventListener
    public void onPreUndeploy(PreUndeployEvent event) {
        log.info("preUndeploy: {}", event);
        processApplicationStopped = true;
    }

    @EventListener
    public void onTaskEvent(TaskEvent event) {
        log.info("taskEvent: {}", event);
    }

}
