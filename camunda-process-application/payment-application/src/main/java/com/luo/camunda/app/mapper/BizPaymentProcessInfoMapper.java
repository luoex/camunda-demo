package com.luo.camunda.app.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.camunda.app.model.entity.BizPaymentProcessInfo;

/**
 * <p>
 * 支付流程 - 业务数据 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2022-01-30
 */
public interface BizPaymentProcessInfoMapper extends BaseMapper<BizPaymentProcessInfo> {

}
