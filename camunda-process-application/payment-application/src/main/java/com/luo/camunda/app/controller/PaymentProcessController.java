package com.luo.camunda.app.controller;

import com.luo.camunda.app.model.entity.BizPaymentProcessInfo;
import com.luo.camunda.app.model.param.PaymentConfirmParam;
import com.luo.camunda.app.model.param.PaymentQueryParam;
import com.luo.camunda.app.model.param.PaymentRequestParam;
import com.luo.camunda.app.service.IBizPaymentProcessInfoService;
import com.luo.camunda.common.model.param.TaskQueryParam;
import com.luo.camunda.common.model.vo.TaskVo;
import com.luo.demo.sc.base.model.result.RespResult;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 支付流程 - Controller
 *
 * @author luohq
 * @date 2022-01-22 18:08
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/payment")
public class PaymentProcessController {



    @Resource
    private IBizPaymentProcessInfoService paymentProcessInfoService;
    /**
     * 开启支付流程
     *
     * @param paymentRequestParam 支付参数
     * @return 响应结果
     */
    @PostMapping
    public RespResult<String> startPayment(@Validated @RequestBody PaymentRequestParam paymentRequestParam) {
        log.info("start payment, param: {}", paymentRequestParam);
        RespResult<String> respResult = this.paymentProcessInfoService.startPaymentProcess(paymentRequestParam);
        log.info("start payment, result: {}", respResult);
        return respResult;
    }

    /**
     * 查询用户待处理任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    @GetMapping("/tasks")
    public RespResult<TaskVo<BizPaymentProcessInfo>> getTasks(TaskQueryParam taskQueryParam) {
        log.info("get payment tasks, param: {}", taskQueryParam);
        RespResult<TaskVo<BizPaymentProcessInfo>> taskVoRespResult = this.paymentProcessInfoService.queryTasks(taskQueryParam);
        log.info("get payment tasks, param: {}", taskQueryParam);
        return taskVoRespResult;
    }

    /**
     * 查询用户历史任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    @GetMapping("/tasks/history")
    public RespResult<HistoricTaskInstance> getHistoryTasks(TaskQueryParam taskQueryParam) {
        log.info("get payment tasks history, param: {}", taskQueryParam);
        RespResult<HistoricTaskInstance> taskVoRespResult = this.paymentProcessInfoService.queryHistoryTasks(taskQueryParam);
        return taskVoRespResult;
    }

    /**
     * 查询支付数据列表
     *
     * @param paymentQueryParam 支付数据查询参数
     * @return 响应结果
     */
    @GetMapping
    public RespResult<BizPaymentProcessInfo> getPayments(PaymentQueryParam paymentQueryParam) {
        log.info("get payment page, param: {}", paymentQueryParam);
        RespResult<BizPaymentProcessInfo>  pageResult = this.paymentProcessInfoService.queryPayments(paymentQueryParam);
        log.info("get payment page, param: {}", paymentQueryParam);
        return pageResult;
    }


    /**
     * 确认支付
     *
     * @param paymentConfirmParam 确认参数
     * @return 响应结果
     */
    @PutMapping("/confirm")
    public RespResult<Integer> confirmPayment(@Validated @RequestBody PaymentConfirmParam paymentConfirmParam) {
        log.info("confirm payment, param: {}", paymentConfirmParam);
        RespResult respResult = this.paymentProcessInfoService.confirmPayment(paymentConfirmParam);
        log.info("confirm payment, result: {}", respResult);
        return respResult;
    }
}
