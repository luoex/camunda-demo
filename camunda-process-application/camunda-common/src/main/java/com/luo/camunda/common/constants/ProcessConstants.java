package com.luo.camunda.common.constants;

/**
 * 流程常量定义
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-01-25 10:47
 */
public class ProcessConstants {
    /**
     * 任务 - column名称
     */
    public static final String COLUMN_ID = "ID_";
    public static final String COLUMN_NAME = "NAME_";
    public static final String COLUMN_CREATE_TIME = "CREATE_TIME_";
    /**
     * 历史任务 - column名称
     */
    public static final String COLUMN_START_TIME = "START_TIME_";
    public static final String COLUMN_END_TIME = "END_TIME_";
    public static final String COLUMN_DURATION = "DURATION_";

    /**
     * 流程常量 - 排序顺序
     */
    public static final String SORT_DESC = "desc";
    public static final String SORT_ASC = "asc";

}
