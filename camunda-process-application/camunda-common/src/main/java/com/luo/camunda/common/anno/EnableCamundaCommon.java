package com.luo.camunda.common.anno;

import com.luo.camunda.common.config.CamundaCommonConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启动Camunda通用工具
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-01-28 16:02
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(CamundaCommonConfig.class)
public @interface EnableCamundaCommon {
}
