package com.luo.camunda.common.model.param;

import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 流程查询参数
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-01-25 14:09
 */
@Data
@Builder
public class ProcessVariablesQueryParam{
    /**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 流程变量名称
     */
    private String variableName;

    /**
     * 租户ID
     */
    private String tenantId;

}
