package com.luo.camunda.common.config;

import com.luo.camunda.common.servcie.CamundaCommonService;
import org.springframework.context.annotation.Bean;

/**
 * Camunda通用工具 - 配置类
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-01-28 16:03
 */
public class CamundaCommonConfig {

    @Bean
    public CamundaCommonService camundaCommonService() {
        return new CamundaCommonService();
    }
}
