package com.luo.camunda.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamundaExternalApplication {

    public static void main(String... args) {
        SpringApplication.run(CamundaExternalApplication.class, args);
    }

}
