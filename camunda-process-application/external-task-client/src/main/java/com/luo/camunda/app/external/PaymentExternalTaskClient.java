package com.luo.camunda.app.external;

import com.luo.camunda.common.utils.CamundaUtils;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Configuration
@Slf4j
public class PaymentExternalTaskClient {

    private final String PRODUCT_NAME = "productName",
            PRODUCT_PRICE = "productPrice",
            PAYMENT_ASSIGNEE = "paymentAssignee",
            PRODUCT_DISCOUNT_PRICE = "productDiscountPrice";

    @ExternalTaskSubscription(topicName = "payment-biz")
    @Bean
    public ExternalTaskHandler paymentBizHandler() {
        return (externalTask, externalTaskService) -> {
            //获取流程变量
            String productName = externalTask.getVariable(PRODUCT_NAME);
            BigDecimal productPrice = CamundaUtils.convertDecimal(externalTask.getVariable(PRODUCT_PRICE));
            String paymentAssignee = externalTask.getVariable(PAYMENT_ASSIGNEE);
            BigDecimal productDiscountPrice = CamundaUtils.convertDecimal(externalTask.getVariable(PRODUCT_DISCOUNT_PRICE));
            log.info("The External Task {} has been checked!", externalTask.getId());
            //完成任务
            externalTaskService.complete(externalTask);
            //完成任务且添加流程变量
            //externalTaskService.complete(externalTask, Variables.putValueTyped("creditScores", creditScoresObject));
            log.info("Payment SUCCESS - paymentAssignee={}, productName={}, productPrice={}, productDiscountPrice={}",
                    paymentAssignee,
                    productName,
                    productPrice,
                    productDiscountPrice);

            //任务失败
            //externalTaskService.handleFailure(
            //        externalTask,
            //        "errorMsg",
            //        "errorDetails",
            //        1,
            //        10L * 60L * 1000L);

        };
    }

}
