# 工作流引擎 Camunda示例

工程结构：
- camunda-platform - camunda webapp管理平台
- camunda-process-application - camunda流程应用示例
  - camunda-common - 流程应用共同模块
  - payment-application - 支付示例流程应用
  - insurance-application - 保险示例流程应用
  - external-task-client - 外部任务示例
  
  
注：该工程依赖 [https://gitee.com/luoex/spring-cloud-demo/spring-cloud-common](https://gitee.com/luoex/spring-cloud-demo/tree/develop/spring-cloud-common) 模块